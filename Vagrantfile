# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_ROOT_DIR = File.dirname(File.expand_path(__FILE__))
VAGRANTFILE_API_VERSION = "2"
DEFAULT_SETTINGS_FILE = "#{VAGRANT_ROOT_DIR}/settings.yaml"

require 'yaml'
require "pathname"
require "#{VAGRANT_ROOT_DIR}/puppet/deep_merge.rb"


settings = YAML.load_file(DEFAULT_SETTINGS_FILE)
Pathname.new("#{VAGRANT_ROOT_DIR}/clusters").children.select(&:directory?).each do |cluster_dir|
  if File.file?("#{cluster_dir}/settings.yaml")
    sub_settings = YAML.load_file("#{cluster_dir}/settings.yaml")
    settings.deep_merge!(sub_settings)
  end
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  settings['clusters'][settings['cluster']]['nodes'].each do |node|
    config.vm.define node["name"] do |srv|
      srv.vm.box = node["box"]
      srv.vm.hostname = node["hostname"]
      srv.vm.network 'private_network',
        ip: "#{settings['network']['ip_prefix']}#{node['ip_postfix']}"

      srv.ssh.insert_key = false

      srv.vm.provider :virtualbox do |vb|
        vb.name = node["name"]
      end

      if node['update_and_install_puppet']
        srv.vm.provision "apt_update",
          type: "shell",
          inline: "sudo apt-get update -qq"

        srv.vm.provision "install_puppet",
          type: "shell",
          inline: "sudo apt-get install -qq puppet"

        srv.vm.provision "install_librarian_puppet",
          type: "shell",
          inline: "sudo gem install librarian-puppet --quiet --silent"

        srv.vm.provision "export_message",
          type: "shell",
          inline: "echo 'Now run these commands \"
            vagrant package --base update_base_box --output maki.box;
            vagrant box remove maki_box;
            vagrant box add --name maki_box maki.box\"'"
      end

      if !node['disable_main_provision_stage']
        srv.vm.provision "run_librarian_puppet_install",
          type: "shell",
          inline: "cd /vagrant/puppet && librarian-puppet install"

        srv.vm.provision "puppet" do |puppet|
          puppet.module_path = "puppet/modules"
          puppet.manifests_path = "puppet/manifests/clusters/#{settings['cluster']}"
          puppet.hiera_config_path = "puppet/hiera.yaml"
          puppet.facter = {
            'facter_test_fact' => 'fact_content',
            'vagrant_settings_cluster' => settings['cluster'],
            'vagrant_settings_node_name' => node["name"],
          }
        end
      end
    end
  end
end
