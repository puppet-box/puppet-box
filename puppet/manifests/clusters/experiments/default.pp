node 'daba.box' {
  include test_module

  file { '/home/vagrant/DABA_ONLY':
    ensure  => file,
    content => 'content\n',
  }

  file { '/home/vagrant/HIERA':
    ensure  => file,
    content => lookup('somevar'),
  }

  file { '/home/vagrant/FACT':
    ensure  => file,
    content => "prefix ${facts['facter_test_fact']} postfix",
  }
}

node /.*\.box/ {
  include test_module

  file { '/home/vagrant/NON_DABA':
    ensure  => file,
    content => 'content\n',
  }

  file { '/home/vagrant/HIERA':
    ensure  => file,
    content => lookup('somevar'),
  }
}

node default {
}
