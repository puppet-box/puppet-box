node 'web.box' {
  include role::web_server_example
}

node 'daba.box' {
  include role::database_server_example
}

node default {
}
