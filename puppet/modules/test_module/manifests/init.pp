class test_module () {

  user { 'herman':
    ensure     => 'present',
    managehome => true,
    password   => pw_hash('herman', 'SHA-512', 'salt'),
  }

  notify { '=== user herman created ===': }
}
