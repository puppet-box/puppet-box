class profile::mysql_user_root (
  $password = 'insecure',
) {

  mysql_user { 'root@%':
    ensure        => 'present',
    password_hash => mysql_password($password),
  }

  mysql_grant { 'root@%/*.*':
    ensure     => 'present',
    options    => ['GRANT'],
    privileges => ['ALL'],
    table      => '*.*',
    user       => 'root@%'
  }
}