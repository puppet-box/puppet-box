class profile::php () {
  class { '::apache::mod::php':
    package_name => 'libapache2-mod-php7.2',
    path         => "${::apache::params::lib_path}/libphp7.2.so",
  }

  file { '/var/www/html/info.php':
    ensure  => file,
    content => '
    <?php phpinfo();
    ',
  }

  package { "php-mysql": }
}
