class profile::example_database_connection (
  $host,
  $username,
  $password,
) {

  class { 'example_database_connection':
    host     => $host,
    username => $username,
    password => $password,
  }

  file { '/var/www/html/index.html':
    ensure => 'absent',
  }
}
