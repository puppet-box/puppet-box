class profile::mysql () {
  class { '::mysql::server':
    override_options => {
      mysqld => {
        # Allow non-localhost connections
        bind-address => '0.0.0.0',
      }
    },
  }
}
