class example_database_connection (
  $host,
  $username,
  $password,
) {
  file { '/var/www/html/example_database_connection.php':
    ensure  => 'file',
    content => epp('example_database_connection/example_database_connection.epp'),
    mode    => '777',
  }
}
